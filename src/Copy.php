<?php

namespace CloverPHP\Godance;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

class Copy extends Command
{
    protected static $defaultName = 'copy';

    protected function configure()
    {
        $this->setDescription('Copy template files to the target project root directory')
            ->addArgument('template', InputArgument::OPTIONAL, 'The template to copy (e.g., ci4 or vue3)')
            ->addOption('template', 't', InputOption::VALUE_OPTIONAL, 'The template to copy (e.g., ci4 or vue3)', 'ci4');
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $template = $input->getArgument('template');
        if (!$template) {
            $output->writeln('Please provide a template name as a positional argument.');
            return Command::FAILURE;
        }

        $templateDir = __DIR__ . '/../.' . $template . '-template';

        if (!is_dir($templateDir)) {
            $output->writeln("Template directory '{$templateDir}' does not exist.");
            return Command::FAILURE;
        }

        try {
            $filesystem = new Filesystem();
            $filesystem->mirror($templateDir, getcwd());
            $output->writeln("Template '{$template}' files copied successfully.");
            return Command::SUCCESS;
        } catch (\Exception $e) {
            $output->writeln("Error copying template '{$template}' files: " . $e->getMessage());
            return Command::FAILURE;
        }
    }
}
